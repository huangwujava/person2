package com.yidu.view;

import com.yidu.domain.Person;
import com.yidu.service.PersonService;
import com.yidu.service.impl.PersonServiceImpl;
import com.yidu.utils.BaseMessage;
import com.yidu.utils.Tools;
import com.yidu.view.vo.PersonVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>@Description:No description </p>
 * <p>@author yidu-huang</p>
 * <p>@Date: 2021/1/13</p>
 * <p>@Time: 11:08</p>
 */
@Controller
@RequestMapping("/person")
public class PersonController {
    @Resource
    PersonService service;

    /**
     * 查询
     * @param vo
     * @return
     */
    @RequestMapping ("/fundAll")
    @ResponseBody
    public Map<String, Object> fundAll(PersonVo vo) {
        List<Person> list=service.fundAll(vo);
        int count=service.fundCount(vo);
        return Tools.getLayUiMap(list,count);
    }

    /**
     * 增加
     * @param vo
     * @return
     */
    @RequestMapping ("/add")
    @ResponseBody
    public BaseMessage add(PersonVo vo) {
        Person po=new Person();
        BeanUtils.copyProperties(vo,po);
        BaseMessage bm = new BaseMessage();
        int row=0;
        if(po.getId()==null) {
            row = service.insert(po);

        }else {
            row=service.updateByPrimaryKeySelective(po);
        }
        bm.setState(row);
        return bm ;
    }
    /**
     * 上屏
     * @param id
     * @return
     */
    @RequestMapping ("/showUpdate")
    @ResponseBody
    public BaseMessage showUpdate(String id) {
        BaseMessage bm = new BaseMessage();
        Person as=service.selectByPrimaryKey(Integer.valueOf(id));
        System.out.println(as.getName());
        bm.setData(as);
        bm.setState(1);
        return bm;
    }
}

