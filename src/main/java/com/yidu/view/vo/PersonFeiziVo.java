package com.yidu.view.vo;

import com.yidu.utils.BaseVo;

import java.util.Date;

/**
 * <p>@Description:No description </p>
 * <p>@author yidu-huang</p>
 * <p>@Date: 2021/1/14</p>
 * <p>@Time: 14:07</p>
 */
public class PersonFeiziVo extends BaseVo {
    private Integer id;

    private Integer pid;

    private String pname;

    private String name;

    private String sex;

    private String birthday_;

    private String photo_;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday_() {
        return birthday_;
    }

    public void setBirthday_(String birthday_) {
        this.birthday_ = birthday_;
    }

    public String getPhoto_() {
        return photo_;
    }

    public void setPhoto_(String photo_) {
        this.photo_ = photo_;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

}
