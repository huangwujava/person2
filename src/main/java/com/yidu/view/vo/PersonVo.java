package com.yidu.view.vo;

import com.yidu.utils.BaseVo;

/**
 * <p>@Description:No description </p>
 * <p>@author yidu-huang</p>
 * <p>@Date: 2021/1/13</p>
 * <p>@Time: 14:05</p>
 */
public class PersonVo extends BaseVo {
    private Integer id;

    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
