package com.yidu.view;

import com.yidu.domain.PersonFeizi;
import com.yidu.service.PersonFeiziService;
import com.yidu.utils.BaseMessage;
import com.yidu.utils.Tools;
import com.yidu.view.vo.PersonFeiziVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.sql.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>@Description:No description </p>
 * <p>@author yidu-huang</p>
 * <p>@Date: 2021/1/14</p>
 * <p>@Time: 14:09</p>
 */
@Controller
@RequestMapping("/PersonFeizi")
public class PersonFeiziController {
    @Resource
    PersonFeiziService service;

    @RequestMapping("/fundAll")
    @ResponseBody
    public Map<String,Object> fundAll(PersonFeiziVo vo){
        List<PersonFeiziVo> list=service.fundAll(vo);
        int count=service.fundCount(vo);
        System.out.println("=========================="+list.get(0).getName());
      return   Tools.getLayUiMap(list,count);
    }
    @RequestMapping("/add")
    @ResponseBody
    public BaseMessage add(PersonFeiziVo vo){
        PersonFeizi po=new PersonFeizi();
        po.setBirthday(Date.valueOf(vo.getBirthday_()));
        po.setPhoto(vo.getPhoto_());
        BeanUtils.copyProperties(vo,po);
        int row=service.insertSelective(po);
        BaseMessage bm=new BaseMessage();
        bm.setState(row);
        return bm;
    }
}
