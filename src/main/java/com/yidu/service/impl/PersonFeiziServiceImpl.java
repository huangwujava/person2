package com.yidu.service.impl;

import com.yidu.dao.PersonFeiziMapper;
import com.yidu.domain.PersonFeizi;
import com.yidu.view.vo.PersonFeiziVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>@Description:No description </p>
 * <p>@author yidu-huang</p>
 * <p>@Date: 2021/1/13</p>
 * <p>@Time: 15:08</p>
 */
@Service
public class PersonFeiziServiceImpl implements com.yidu.service.PersonFeiziService {

    @Resource
    PersonFeiziMapper personFeiziMapper;
    @Override
    public int deleteByPrimaryKey(Integer id) {
        return personFeiziMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(PersonFeizi record) {
        return personFeiziMapper.insert(record);
    }

    @Override
    public int insertSelective(PersonFeizi record) {
        return personFeiziMapper.insertSelective(record);
    }

    @Override
    public PersonFeizi selectByPrimaryKey(Integer id) {
        return personFeiziMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(PersonFeizi record) {
        return personFeiziMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(PersonFeizi record) {
        return personFeiziMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<PersonFeiziVo> fundAll(PersonFeiziVo record) {
        return personFeiziMapper.fundAll(record);
    }

    @Override
    public int fundCount(PersonFeiziVo record) {
        return personFeiziMapper.fundCount(record);
    }

}
