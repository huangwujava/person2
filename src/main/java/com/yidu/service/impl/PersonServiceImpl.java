package com.yidu.service.impl;

import com.yidu.dao.PersonMapper;
import com.yidu.domain.Person;
import com.yidu.service.PersonService;
import com.yidu.view.vo.PersonVo;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>@Description:No description </p>
 * <p>@author yidu-huang</p>
 * <p>@Date: 2021/1/13</p>
 * <p>@Time: 11:01</p>
 */
@Service
public class PersonServiceImpl implements PersonService{
    @Resource
    PersonMapper personMapper ;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return personMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Person record) {
        return personMapper.insert(record);
    }

    @Override
    public int insertSelective(Person record) {
        return personMapper.insertSelective(record);
    }

    @Override
    public Person selectByPrimaryKey(Integer id) {
        return personMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Person record) {
        return personMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Person record) {
        return personMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<Person> fundAll(PersonVo vo) {
        return personMapper.fundAll(vo);
    }

    @Override
    public int fundCount(PersonVo vo) {
        return personMapper.fundCount(vo);
    }
}
