package com.yidu.service;

import com.yidu.domain.Person;
import com.yidu.view.vo.PersonVo;

import java.util.List;

public interface PersonService {
    /**
     * 根据id删除
     * @param id
     * @return
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 增加
     * @param record
     * @return
     */
    int insert(Person record);

    /**
     * 选择性增加
     * @param record
     * @return
     */
    int insertSelective(Person record);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    Person selectByPrimaryKey(Integer id);

    /**
     * 修改
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(Person record);

    /**
     * 选择性修改
     * @param record
     * @return
     */
    int updateByPrimaryKey(Person record);
    /**
     * 查询所有
     * @param vo
     * @return
     */
    List<Person> fundAll(PersonVo vo);

    /**
     * 查询行数
     * @param vo
     * @return
     */
    int fundCount(PersonVo vo);
}
