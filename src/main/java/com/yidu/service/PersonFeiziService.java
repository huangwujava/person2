package com.yidu.service;

import com.yidu.domain.PersonFeizi;
import com.yidu.view.vo.PersonFeiziVo;

import java.util.List;

public interface PersonFeiziService {
    int deleteByPrimaryKey(Integer id);

    int insert(PersonFeizi record);

    int insertSelective(PersonFeizi record);

    PersonFeizi selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PersonFeizi record);

    int updateByPrimaryKey(PersonFeizi record);

    List<PersonFeiziVo> fundAll(PersonFeiziVo record);

    int fundCount(PersonFeiziVo record);
}
