package com.yidu.utils;

/**
 * <p>@Description: 消息类</p>
 * <p>@Author yidu-liandyao</p>
 * <p>@Date: 2021-01-06</p>
 * <p>@Time: 9:27</p>
 */
public class BaseMessage {
    private int state ;//状态
    private String msg ; //消息
    private Object data ;//数据

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
