package com.yidu.utils;

/**
 * <p>@Description: 基础的分页VO</p>
 * <p>@Author yidu-liandyao</p>
 * <p>@Date: 2021-01-12</p>
 * <p>@Time: 10:10</p>
 */
public class BaseVo {
    private Integer page =1 ; //第几页
    private Integer limit =10 ; //每页多少行
    private Integer startRows ;//开始的行数

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getStartRows() {
        startRows = (page-1)*limit;
        return startRows;
    }

    public void setStartRows(Integer startRows) {
        this.startRows = startRows;
    }
}
