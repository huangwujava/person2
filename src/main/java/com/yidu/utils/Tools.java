package com.yidu.utils;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * <p>@Description: 常用的工具类</p>
 * <p>@Author yidu-liandyao</p>
 * <p>@Date: 2020-11-17</p>
 * <p>@Time: 9:00</p>
 */
public class Tools {

    /**
     * 传入一个String,判断该对象是否为空
     * @param obj
     * @return 如果为空返回true,否则返回false
     */
    public static boolean isEmpty(String obj){

        if(obj==null || "".equals(obj)){
            return true ;
        }else{
            return false ;
        }

    }

    static Random rd = new Random();

    public static String getRandom(){
        long time = System.currentTimeMillis();

        time = time/(rd.nextInt(10)+1);

        int num = rd.nextInt(10000000);

        String str = time+"_"+num ;
        return str ;
    }

    /**
     * 返回LAYUI需要的格式
     * @param list
     * @param count
     * @return
     */
    public static Map<String,Object> getLayUiMap(List list, int count){
        Map<String,Object> map = new HashMap<>();
        map.put("msg","");
        map.put("code",0);
        map.put("data",list);
        map.put("count",count);
        return map ;
    }


    public void test1(){
        System.out.println("====");
    }

}
